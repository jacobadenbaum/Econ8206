include("MLE.jl")
using KernelDensity
using DataFrames

########################################################################
#################### Estimation ########################################
########################################################################


"""
```
BurdettMortensen(data [;δ, λ0, λ1])
```
Given a set of parameters and data from the BHPS survey of workers,
constructs a Burdett Mortensen maximum likelihood estimator.

Usage:

    ```
    d = BurdettMortensen(data)
    
    estimate!(d, ones(3))
    
    @show d
    
    @show log_likelihood(d)
    ```
"""
type BurdettMortensen <: MaximumLikelihood
    parameters
    # Parameters needing to be set
    #   δ   # The separation rate
    #   λ0  # Job finding rate from unemployment
    #   λ1  # job finding rate from employment
    data
    std_errors      # Standard Errors
    _logw1_sorted   # Presorted for fast computation
    _logw1_length   # Pre-computed for fast-computation
    _Gdens          # Pre-computed KDE estimate

    BurdettMortensen(parameters, data) = new(parameters, data)
end

function BurdettMortensen(data; kwargs...)
    
    # Construc the model instance using the parameters passed as keyword
    # arguments as well as the data
    args = Dict(kwargs)
    d = BurdettMortensen(args, data)
    
    # Prep the log-wage data for quick computation of densities
    construct_edf!(d)
    return d
end

# Construct Empirical Distributions
function construct_edf!(d::BurdettMortensen)
    # Compute the sorted distribution of log wages
    logw1 = dropna(d.data[:logw1])
    sort!(logw1)

    d._logw1_sorted = logw1
    d._logw1_length = length(logw1)
    
    # Compute a kernel density estimate of the cross sectional distribution
    # of log wages using least-squares cross-validation to select the
    # bandwidth parameter
    d._Gdens = InterpKDE(kde_lscv(logw1))
    
    return d
end

"""
```
G(model, w)
```
The empirical CDF of the initial (spell-1) distribution of log wages
(logw1)
"""
function G(d::BurdettMortensen, w)
    idx = searchsortedfirst(d._logw1_sorted, w)
    return (idx-1)/d._logw1_length
end

"""
```
Gdens(model, w)
```
Calls pre-computed kernel-density estimate of the cross-sectional wage
distribution.  Uses Quadratic spline interpolation for fast
indexing between reference points
"""
function Gdens(d::BurdettMortensen, w)
    return pdf(d._Gdens, w)
end

# Given a set of parameters, impute the distribution of accepted job
# offers from the cross-sectional distribution of job offers
function F(d::BurdettMortensen, x)
    λ1, δ = d[:λ1], d[:δ]
    Gx = G(d, x)
    return (δ+λ1)*Gx/(δ + λ1*Gx)
end

# Imputed density function of accepted wages -- just the derivative of
# the cumulative distribution
function Fdens(d::BurdettMortensen, x)
    λ1, δ = d[:λ1], d[:δ]
    
    Gx = G(d, x)
    gx = Gdens(d, x)
    
    low = (δ + λ1*Gx)
    dlow = λ1*gx

    high =(δ+λ1)Gx
    dhigh =(δ+λ1)gx 

    return (low*dhigh - high*dlow)/low^2
end

"""
```
log_likelihood(model, obs)
```
Computes the log-likelihood contribution of the given observation using
the parameters specified by the specified instance of the
BurdettMortensen model.  Obs must be indexable like a Dict.  
"""
function log_likelihood(d::BurdettMortensen, obs)
    λ0, λ1, δ = d[:λ0], d[:λ1], d[:δ]
   
    pmin = minimum([λ0, λ1, δ])
    if pmin  < 0
        return 1e20*pmin
    end

    # Job spell duration
    di = obs[:spelldur1]
    if obs[:e1] == 0 
        # Censoring
        c = (isna(obs[:e2]) || obs[:e2] == 0) ? 1. : 0.
        
        # Compute likelihood
        scale = log(δ/(δ + λ0))
        duration_dens = log(λ0^(1-c)*exp(-λ0*di))
        val = scale+duration_dens
        return val
    else
        # Censoring
        c = 1 - obs[:j2j] - obs[:j2u]

        # Compute likelihood
        w  = obs[:logw1]
        di = obs[:spelldur1]
        Fbar = 1-F(d,w)
        scale = log(λ0/(δ+λ0))
        wage_dens = log(Gdens(d, w))
        duration_dens = (c==0 ? log((δ+λ1*Fbar)):0) + -(δ+λ1*Fbar)*di
        term1_dens = log((δ/(δ+λ1*Fbar))^obs[:j2u])
        term2_dens = log((λ1*Fbar/(δ+λ1*Fbar))^obs[:j2j])
        
        val = scale+wage_dens+duration_dens+term1_dens+term2_dens
        return val
    end
end


# Unpack the parameters from x into d
function unpack!(d::BurdettMortensen, x::Vector)
    d[:δ]  = x[1]
    d[:λ0] = x[2]
    d[:λ1] = x[3]
    return d
end

function unpack(d::BurdettMortensen, x::Vector)
    params = Dict()
    params[:δ]  = x[1]
    params[:λ0] = x[2]
    params[:λ1] = x[3]
    return params
end

# pack the parameters from d into a vector x
pack(d::BurdettMortensen) = vcat(d[:δ], d[:λ0], d[:λ1]) 
    
    

########################################################################
#################### Equilibrium Objects ###############################
########################################################################
"""
```
productivity(model, wage)
```
Computes the productivity of the firm who posts the given wage in the
equilibrium implied by the parameters of the model.

Note: This function takes wages -- NOT log-wages. 

If wage is NA, returns NA.
"""
function productivity(d::BurdettMortensen, w)
    if isna(w)
        return NA
    else
        Gw = G(d, w)
        gw = Gdens(d, w)
        κ1 = d[:δ]/d[:λ0]
        return exp(w) + (1 + κ1*Gw)/(2*κ1*gw)
    end
end
