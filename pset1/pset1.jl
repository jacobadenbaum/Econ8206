#=
Title:  pset1.jl
Author: Jacob Adenbum
Date:   9/26/2017

This script uses the maximum likelihood routines written in
BurdettMortensen.jl to estimate a version of the BM model on the BHPS
pannel of household employment.  
=#

# Load the code for the MLE Estimator
include("BurdettMortensen.jl")

# Plotting
using PyPlot, DataFrames, MAT, Tables

########################################################################
#################### Data Cleaning #####################################
########################################################################

# Read in the Data
df = readtable("BM_data.csv")

# Check my understanding of the job codings...
check = by(df, :pid) do x
    if isna(x[1,:e2])
        return true
    elseif x[1,:u2j] == 1 && x[1,:e2] == 1 && x[1,:e1] == 0
        return true
    elseif x[1,:j2j] == 1 && x[1,:e1] == 1 && x[1,:e2] == 1
        return true
    elseif x[1,:j2u] == 1 && x[1,:e1] == 1 && x[1,:e2] == 0
        return true
    else
        return false
    end
end
# This is weird.  There are 25 observations where the j2u j2j and u2j
# codings don't match up with the employment codings.  It looks to me as
# though the e2 coding is correct, since in all of these cases, the
# worker is has no wage data.  I think that we *shouldn't* use the
# transition coding.  Instead, we should recode based on e1 and e2

function update_coding(row)
    if isna(row[1,:e2])
        u2j = 0.0
        j2j = 0.0
        j2u = 0.0
    elseif row[1,:e1] == row[1,:e2]
        u2j = 0.0
        j2j = 1.0
        j2u = 0.0
    elseif row[1,:e1] == 0 && row[1,:e2] == 1
        u2j = 1.0
        j2j = 0.0
        j2u = 0.0
    elseif row[1,:e1] == 1 && row[1,:e2] == 0
        u2j = 0.0
        j2j = 0.0
        j2u = 1.0
    else
        throw(AssertionError("Something went wrong"))
    end
    return DataFrame(u2j=u2j, j2j=j2j, j2u=j2u)
end


# Make the new codes
codes = by(df, :pid, update_coding)
# Delete the bad codes
delete!(df, [:u2j, :j2j, :j2u])
# Merge the new ones in
df = join(df, codes, on=:pid)

# But the e1 and e2 categories behave like they should
check2 = df[.!isna.(df[:e2]),:]
@assert sum((check2[:e1] == check2[:e2]) .& (check2[:e2] == 0)) == 0
    # in other words, if e1 and e2 are the same, then there was a job to
    # job transition

########################################################################
#################### Construct the Model ###############################
########################################################################

d = BurdettMortensen(df)

########################################################################
#################### Descriptive Statistics ############################
########################################################################

## Number of observations
N = size(df, 1)

## Compute the sex ratio
sex_ratio = mean(df[:sex])*100 
    # 54% of the sample is male

## Compute the sample unemployment rate:
# We'll calculate the percentage of the people who were unemployed at
# first contact

# overall
unemployment = mean(1-df[:e1])

# By sex
unemployment_by_sex = by(df, :sex) do x
    pct = mean(1-x[:e1])*100
    return DataFrame(unemployed=pct)
end

# By Education
unemployment_by_educ = by(df, :educ) do x
    pct = mean(1-x[:e1])*100
    return DataFrame(unemployed=pct)
end


## What proportion of initial spells are right-censored?  
right_censored = mean(isna.(df[:e2]))*100
right_censored_by_spell1 = by(df, :e1) do x
    DataFrame(pct_censored=mean(isna.(x[:e2])*100))
end

# We can also compute the empirical distribution of wage offers by
# looking at workers who transition from unemployment to employment
# (since in this model, they will *always* choose to accept any offer)
function Fhat(df, w)
    new_wages= df[df[:u2j] .== 1,:logw2] 
    return mean(dropna(new_wages).<=w)
end

## Plot the empirical CDF and the estimated empirical pdf
logw1 = dropna(df[:logw1])
wmin = .9*minimum(logw1)
wmax = 1.1*maximum(logw1)
wgrid = linspace(wmin, wmax, 100)

# Empirical CDF
fig, ax = subplots()
plot(wgrid, G.(d, wgrid), label="\$ \\hat G(w)\$ (Wages)")
plot(wgrid, Fhat.(df, wgrid), label="\$ \\hat F(w)\$ (Wage Offers)")
title("Empirical CDF of Cross-Sectional Wages and Wage Offers")
xlabel("Log Wages")
legend()
savefig("cdf_wages.pdf")
close()

# Plot Ratio
fig, ax = subplots()
κ1 = (Fhat.(df, wgrid) - G.(d, wgrid))./(G.(d, wgrid).*(1-Fhat.(df,wgrid)))
plot(wgrid, κ1)
title("F/G Relationship")
savefig("FG_relationship.pdf")
close()

# Empirical PDF
fig, ax = subplots()
plot(wgrid, Gdens(d, wgrid), label="\$g(w)\$")
title("Empirical PDF of Cross-Sectional Wages")
xlabel("Log Wages")
legend()
savefig("pdf_wages.pdf")
close()

### Section I.4.
##  Create a variable categorizing logw1 into 25 percentile bins and a
#   variable containing the mean spell-1 duration within each of these
#   bins.  

logw1 = dropna(df[:logw1])
quantiles = linspace(0,1,26)
edges = [quantile(logw1, q) for q in quantiles]

df[:quantile] = 0.0
for i=1:25
    if i > 1
        df[edges[i] .<= df[:logw1] .< edges[i+1], :quantile] = quantiles[i]
    end
end
durations  = by(df, :quantile) do x
    DataFrame(mean_duration=mean(x[:spelldur1]))
end

fig, ax = subplots()
plot(durations[:quantile]*100, durations[:mean_duration])
xlabel("Percentile of Log Wages")
ylabel("Mean Duration of Employment Spell")
savefig("wages_duration.pdf")
close()

########################################################################
#################### Estimate the Model ################################
########################################################################

# Estimate using Maximum Likelihood
estimate!(d, ones(3))

# Compute Standard Errors
bootstrap!(d, 1000)

# Make the Table
key = [:δ, :λ0, :λ1]
t = Table()
add_column!(t, "Parameters",
    key,
    [d.parameters[k] for k in key],
    fmt="{:4f}")
add_column!(t, "Std Err",
    key,
    [d.std_errors[k] for k in key],
    fmt="({:4f})")

# Replacement characters for unicode to tex
replacements = Dict(
    "λ0"=>"\$\\lambda_0\$",
    "λ1"=>"\$\\lambda_1\$",
    "δ"=>"\$\\delta\$"
)

write_tex("parameters.tex", t, replacements)

########################################################################
#################### Check the Simulated Data ##########################
########################################################################

# Load the data into a Dict
f = matread("BM_data_simulated.mat")

# Fix the data so it's column vectors instead of one-column matrices
for key in keys(f)
    f[key] = f[key][:]
end

# Construct the dataframe with the simulated data
dfsim = DataFrame(f)

# Reconstruct some of the variables I need for the estimation routine
dfsim[:e1] = dfsim[:e]
dfsim[:e2] = dfsim[:j2j] + dfsim[:u2j]

# Set the data to missing if it's coded as 1
idx = dfsim[:logw1] .== -1
dfsim[idx, :logw1] = NA

# Construct the model object
dsim = BurdettMortensen(dfsim)

# Estimate the model using the parameter estimate from the data as the
# starting value
estimate!(dsim, pack(d))
bootstrap!(dsim, 1000)

# Make the Table
t = Table()
add_column!(t, "True Values",
    [:δ, :λ0, :λ1],
    [0.01, 0.1, 0.05],
    fmt="{:4f}")
add_column!(t, "Estimated",
    keys(dsim.parameters),
    values(dsim.parameters),
    fmt="{:4f}")
add_column!(t, "Std Err",
    keys(dsim.std_errors),
    values(dsim.std_errors),
    fmt="({:4f})")

write_tex("simulated_parameters.tex", t, replacements)

########################################################################
#################### Playing around with the model #####################
########################################################################

# Compute model's predicted unemployment:
unemployment_predicted = d[:δ]/(d[:δ] + d[:λ0])
    # We get 0.0786
    # Note the difference between this and the unemployment rate in the
    # sample of 0.0534
    # This is probably due to the fact that our model assumes that all
    # the workers are identical, whereas we know that the unemployment
    # rate actually differs quite a bit across different subgroups of
    # workers.
    # There is also considerable variation in the unemployment rate
    # across years in the sample, which cannot be accounted for in our
    # model.   


# Plot the predicted CDF and PDF of F and G from the model.
wmin = .9*minimum(logw1)
wmax = 1.1*maximum(logw1)
wgrid = linspace(wmin, wmax, 100)
fig, ax = subplots()
plot(wgrid, G.(d, wgrid), label="\$ \\hat G(w)\$ (Wages)")
plot(wgrid, Fhat.(df, wgrid), label="\$ F(w)\$ (Empiricial Wage Offers)")
plot(wgrid, F.(d, wgrid), label="\$ \\hat F(w)\$ (Estimated Wage Offers)")
title("Empirical and Estimated CDF of Cross-Sectional Wages and Wage Offers")
xlabel("Log Wages")
legend()
savefig("cdf_wages_estimated.pdf")
close()

# Now do the probability distribution functions
new_wages= dropna(df[df[:u2j] .== 1,:logw2] )
Fhatdens = InterpKDE(kde_lscv(new_wages))
fig, ax = subplots()
plot(wgrid, Gdens.(d, wgrid), label="\$ \\hat g(w)\$ (Wages)")
plot(wgrid, pdf(Fhatdens, wgrid), label="\$ f(w)\$ (Empiricial Wage Offers)")
plot(wgrid, Fdens.(d, wgrid), label="\$ \\hat f(w)\$ (Estimated Wage Offers)")
title("Empirical and Estimated PDF of Cross-Sectional Wages and Wage Offers")
xlabel("Log Wages")
legend()
savefig("pdf_wages_estimated.pdf", bbox_inches="tight")
close()

# What is the implied distribution of firm productivities in the wage
# posting model?
prod = log(productivity.(d, logw1))
prod_dens = InterpKDE(kde_lscv(prod))

sort!(prod)
Fprod(x) = searchsortedfirst(prod, x)/length(prod)

fig, ax = subplots()
l1 = plot(wgrid, Fprod.(wgrid), label="Firm Productivity")
l2 = plot(wgrid, G.(d, wgrid), label="\$ \\hat G(w) \$ (Wages)", color="red")
xlabel("Log Wages")
legend(loc=0)
savefig("productivity_wages.pdf")
close()
