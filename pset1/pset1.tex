\documentclass[header.tex]{subfiles}

\usepackage{minted}
% General Minted Settings
\setminted{ encoding=utf-8, 
    linenos,
    frame=lines,
    numbersep=.5em,
    framesep=2mm,
    baselinestretch=1,
    bgcolor=white,
    fontsize=\footnotesize}


\usepackage{fancyhdr}
\lhead{Jacob Adenbaum}
\chead{Econ 8601}
\rhead{\today}

\title{Problem Set 1}
\author{Jacob Adenbaum\\
Econ 8601}

\pagestyle{fancy}
\begin{document}

\maketitle

\section{Preliminary Data Analysis}

The BHPS data is a compressed-pannel that follows workers over two contacts -- an
intial contact when their wage is recorded, and a second contact either
at the end of a pre-specified period, or when they next transitioned
into a new job or unemployment. It records certain demographic data
about them (such as their age, employment sector, sex, race, and
education level) as well as their wage in their current job whenever
they are employed.  It is well suited for estimating the Burdett
Mortensen model by virtue of its underlying structure -- it records the
workers' wages and employment status over the entire course of a job
spell, allowing us to exploit the data it provides on the duration of
their job spell in order to identify the underlying parameters of the
model.  

At a basic level, the data has records on 2263 individuals followed over
the course of the various waves of the study. 54.1\% of the sample is
male, and while there is considerable variation in the rate of
unemployment year to year (likely due to relatively small samples in
each year-cohort), the unemployment rate is relatively similar between
men and women (men have an unemployment rate of 7.74\%, while only
3.76\% of women are unemployed).  

There is less variation in the unemployment rate by education levels:
while both workers who took less than A-levels and those who took
A-levels have unemployment rates close to 6\% (6.67\% and 6.09\%
respectively), workers who received some higher education had an
unemployment rate of 3.34\%.  Unsurprisingly, the unemployment rate
appears to be decreasing in the amount of education the worker has.  

A rather considerable number of our workers do not change their
employment status while we observe them.  A full 47.7\% of the sample is
right-censored, although only 30.6\% of unemployed workers remain
unemployed for the whole sample; the bulk of the right censoring comes
from the employed workers, who remain in their original jobs over the
whole observation period at a rate of 48.8\%.  

We can identify the cross-sectional distribution of wages
nonparametrically from the observed wages in the data, as well as the
distribution of wage-offers which can be identified off of the
distribution of wages observed in unemployment-to-job transitions (since
in the Burdett Mortensen model, unemployed workers all have the same
reservation wage and so they will take any wage offered).  We plot these
two distributions in Figure \ref{fig:cdf_wages}.  Of particular note is
the roughness of the estimate of the estimated wage offer distribution
function $\hat F$.  This is likely due to the relatively low proportion
of workers who are unemployed at the start of the sample -- as a result,
we are identifying the distribution off of relatively little data, and
so it is to be expected that it will be quite noisy.  It may be better
to use the implied distribution of wage offers as a function of the
cross-sectional distribution of wages $\hat G$ once we have estimated
all of the structural parameters of the model.  

\begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{cdf_wages_estimated.pdf}
    \caption{The cumulative distribution function of wages and wage
    offers, identified non-parametrically from the data}
    \label{fig:cdf_wages}
\end{figure}

\section{Estimation}

We estimate the model by constructing the likelihood function for each
observation, and computing the parameters which maximize the log
likelihood of the model.  For a worker, we calculate the conditional
likelihood function

\begin{align*}
    l(x_i \; | \; e_i = 1)
=&g(y_{i1} \times 
\left( 
    \left( 
        \delta + \lambda_1 \overline F(y_{yi})
    \right)^{1-c_i}
    e^{ 
        -(\delta + \lambda_1 \overline F(y_{1i})) d_i
    }
\right) \\
&\times  
\left( \delta \over \delta + \lambda_1 \overline F(y_{1i})
\right)^{\tau_{JU_i}}
\times 
\left( \lambda_1 \overline F(y_{1i})
\over \delta + \lambda_1 \overline F(y_{1i})\right)^{\tau_{JJ_i}}\\
l(x_i \; | \; e_i=0)
=& \lambda_0^{1-c_i} e^{-\lambda_0 d_i}
\end{align*}
and where the conditional likelihood function is just a combination of
the two, scaled by the probability of employment or unemployment:
$$ l(x_i)
= \left( {\lambda_0 \over \delta + \lambda_0} l(x_i | e_i=1)
\right)^{e_i} \times
\left( {\delta \over \delta + \lambda_0} l(x_i|e_i=0) \right)^{1-e_i}$$
We estimate this model from the data, and then calculate the standard
errors by bootstrap.  This requires assuming that each individual is a
random draw from the true distribution of people, uncorrelated with the
others in the sample.  Under this assumption, we can treat the data as a
nonparametric estimator of the true data generating process, and
generate repeated samples by sampling the rows of our data (with
replacement).  We use 1,000 draws in order to construct and estimate of
the sampling distribution of our parameters, and report the standard
error as the standard deviation of each parameter's sampling
distribution.  We report the estimated parameters and standard errors in
Table \ref{tab:estimated}.

\begin{table}
    \centering
    \input{parameters.tex}
    \caption{Estimated Structural parameters from the Burdett-Mortensen
    model estimated on BHPS data.}
    \label{tab:estimated}
\end{table}

We also read in the simulated data, generated with the true parameters
$\lambda_0 = 0.1$, $\lambda_1 = 0.05$, and $\delta = 0.01$ and compute
the Burdett Mortensen model on this new data, as well as the standard
errors using Bootstrap (again using 1,000 resamplings). We report the
results in Table \ref{tab:simulated}. In fact, we do quite a good job
recovering the original parameters -- our parameter estimates fall
within one standard deviation of the true parameter in each case.   

\begin{table}
    \centering
    \input{simulated_parameters.tex}
    \caption{Estimated Structural parameters from the Burdett-Mortensen
    model estimated on the simulated data.}
    \label{tab:simulated}
\end{table}

\section{Playing around with the model}

Using our estimated model, we can compute the unemployment rate as $u=
{\delta \over \delta + \lambda_0}$.  Our estimates imply that this rate
will be 7.86\%.  Note the difference between this and the sample
unemployment rate.  This perhaps suggests that our model is misspecified
-- we might consider estimating the model only on certain subgroups
(perhaps by education level).  

We can calculate kernel density estimates of the distribution of wages
and the implied sampling distribution (using least-squares
cross-validation to compute the optimal bandwidth parameter).  We also
compare these estimates with the wage offers that we observe in the data
using unemployment-to-employment transitions.  As we see in Figure
\ref{fig:pdf_wages}, there is a sharp deviation between the observed
wage offers and the distribution of wages implied by the cross-section
and our parameter estimates.  The Burdett Mortensen model observes a
large mass of workers whose wages are bunched at the lower-end of the
distribution, and infers that they must have received those low wages as
initial offers out of unemployment.  However, this is almost certainly a
result of model misspecification -- their low wages are likely a result
of individual heterogeneity among the workers.  We would likely
alleviate this problem if we reestimated the model on each education
level separately.  

\begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{pdf_wages_estimated.pdf}
    \caption{The density of wages, observed wages, and the implied
    distribution of wage offers.}
    \label{fig:pdf_wages}
\end{figure}

We can also compute the distribution of firm productivity that
rationalizes the observed wage distribution in the Burdett Mortensen
model, by computing 
$$ p = w + {1 + \kappa_1 \hat G(w) \over 2\kappa_1 \hat g(w)}$$
We compute it and plot its distribution function against the CDF of firm
wages in Figure \ref{fig:productivity}. Surprisingly, there appears to
be no difference between the estimated distribution of the two.  All of
the heterogeneity in worker's wages is being explained by firm
productivity -- none of it is being accounted for by the search
frictions.  

\begin{figure}[h!]
    \centering
    \includegraphics[width=.75\textwidth]{productivity_wages.pdf}
    \caption{The relationship between log firm productivity and the
    estimated CDF of log wages.}
    \label{fig:productivity}
\end{figure}

\section{Code}
The code to produce the output found in this problem set is organized
into four main files:
\begin{enumerate}[label=\arabic*.]
    \item \verb|pset1.jl|: A script which loads the data, computes the
        summary statistics, estimates the model, and makes the output

    \item \verb|BurdettMortensen.jl|: Defines the
        \verb|BurdettMortensen| type and provides methods to compute the
        likelihood function of it, estimate the underlying parameters
        with maximum likelihood, and compute bootstrap standard errors.  


    \item \verb|Models.jl|: Provides several high-level model types

    \item \verb|Tables.jl|: Some output code to make pretty latex tables
\end{enumerate}
\end{document}
